<?php

/* HelpDeskBundle:Incidence:edit.html.twig */
class __TwigTemplate_ecb7eb74cc8fd059b0292bc5c67b5a5e2b14b5c1e2259ca409d4444c48fe1702 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f8e881c63cdfdf96fbe50bdb9fe05c2c9a0e74cf5a6be279f36bcbd8d32e93a1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f8e881c63cdfdf96fbe50bdb9fe05c2c9a0e74cf5a6be279f36bcbd8d32e93a1->enter($__internal_f8e881c63cdfdf96fbe50bdb9fe05c2c9a0e74cf5a6be279f36bcbd8d32e93a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "HelpDeskBundle:Incidence:edit.html.twig"));

        $__internal_923bedc1f083332e3ed6ed0b7921a5f89567e4d93220fd0433506cb3714552b3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_923bedc1f083332e3ed6ed0b7921a5f89567e4d93220fd0433506cb3714552b3->enter($__internal_923bedc1f083332e3ed6ed0b7921a5f89567e4d93220fd0433506cb3714552b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "HelpDeskBundle:Incidence:edit.html.twig"));

        // line 1
        echo "<style type=\"text/css\">
    label {
        display: inline-block;
        width:125px;
        vertical-align: top;
       } 
    input, textarea {
       width:200px;
       }
    input:focus {
        border: 2px solid #000;
        background: #F3F3F3;
    }
    textarea:focus {
        border: 2px solid #000;
        background: #F3F3F3;
    }
</style>

<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
    </head>
    <body>
        <h1>Detail for incidence</h1>
        ";
        // line 27
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form');
        echo "
    </body>
</html>
";
        
        $__internal_f8e881c63cdfdf96fbe50bdb9fe05c2c9a0e74cf5a6be279f36bcbd8d32e93a1->leave($__internal_f8e881c63cdfdf96fbe50bdb9fe05c2c9a0e74cf5a6be279f36bcbd8d32e93a1_prof);

        
        $__internal_923bedc1f083332e3ed6ed0b7921a5f89567e4d93220fd0433506cb3714552b3->leave($__internal_923bedc1f083332e3ed6ed0b7921a5f89567e4d93220fd0433506cb3714552b3_prof);

    }

    public function getTemplateName()
    {
        return "HelpDeskBundle:Incidence:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 27,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<style type=\"text/css\">
    label {
        display: inline-block;
        width:125px;
        vertical-align: top;
       } 
    input, textarea {
       width:200px;
       }
    input:focus {
        border: 2px solid #000;
        background: #F3F3F3;
    }
    textarea:focus {
        border: 2px solid #000;
        background: #F3F3F3;
    }
</style>

<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
    </head>
    <body>
        <h1>Detail for incidence</h1>
        {{ form(form) }}
    </body>
</html>
", "HelpDeskBundle:Incidence:edit.html.twig", "/home/racso/Master/Frameworks/symfony-standard/src/HelpDeskBundle/Resources/views/Incidence/edit.html.twig");
    }
}
