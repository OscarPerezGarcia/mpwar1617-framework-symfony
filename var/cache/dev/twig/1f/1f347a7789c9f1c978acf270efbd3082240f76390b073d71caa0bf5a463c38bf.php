<?php

/* HelpDeskBundle:Incidence:incidenceCreate.html.twig */
class __TwigTemplate_085a288fa5e8e37679ad6463cc58cc1c07cb501e90f8d7d2b3d21e02c77d5d40 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_304767c6f4acbc8ab58a0a1b5eac284d85faa73df7ea9a053bb94027ee1ae421 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_304767c6f4acbc8ab58a0a1b5eac284d85faa73df7ea9a053bb94027ee1ae421->enter($__internal_304767c6f4acbc8ab58a0a1b5eac284d85faa73df7ea9a053bb94027ee1ae421_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "HelpDeskBundle:Incidence:incidenceCreate.html.twig"));

        $__internal_7da3b955518a8b6d3d3cc134ca12243d7469174e6795d09879a6746d40686e4c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7da3b955518a8b6d3d3cc134ca12243d7469174e6795d09879a6746d40686e4c->enter($__internal_7da3b955518a8b6d3d3cc134ca12243d7469174e6795d09879a6746d40686e4c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "HelpDeskBundle:Incidence:incidenceCreate.html.twig"));

        // line 1
        echo "<style type=\"text/css\">
    label {
        display: inline-block;
        width:125px;
        vertical-align: top;
       } 
    input, textarea {
       width:200px;
       }
    input:focus {
        border: 2px solid #000;
        background: #F3F3F3;
    }
    textarea:focus {
        border: 2px solid #000;
        background: #F3F3F3;
    }
</style>

<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
    </head>
    <body>
        <h1>Create incidence</h1>
        ";
        // line 27
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form');
        echo "
    </body>
</html>

";
        
        $__internal_304767c6f4acbc8ab58a0a1b5eac284d85faa73df7ea9a053bb94027ee1ae421->leave($__internal_304767c6f4acbc8ab58a0a1b5eac284d85faa73df7ea9a053bb94027ee1ae421_prof);

        
        $__internal_7da3b955518a8b6d3d3cc134ca12243d7469174e6795d09879a6746d40686e4c->leave($__internal_7da3b955518a8b6d3d3cc134ca12243d7469174e6795d09879a6746d40686e4c_prof);

    }

    public function getTemplateName()
    {
        return "HelpDeskBundle:Incidence:incidenceCreate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 27,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<style type=\"text/css\">
    label {
        display: inline-block;
        width:125px;
        vertical-align: top;
       } 
    input, textarea {
       width:200px;
       }
    input:focus {
        border: 2px solid #000;
        background: #F3F3F3;
    }
    textarea:focus {
        border: 2px solid #000;
        background: #F3F3F3;
    }
</style>

<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
    </head>
    <body>
        <h1>Create incidence</h1>
        {{ form(form) }}
    </body>
</html>

", "HelpDeskBundle:Incidence:incidenceCreate.html.twig", "/home/racso/Master/Frameworks/symfony-standard/src/HelpDeskBundle/Resources/views/Incidence/incidenceCreate.html.twig");
    }
}
