<?php

/* HelpDeskBundle:Resolution:resolutionCreate.html.twig */
class __TwigTemplate_f81b01e8578339ab834f6a0d6053be7f85c29147dd5c62aa557e473922d7c2d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7950653bc2f1f871b1b31bf0eb67a0fa6e9132d1b0abadf8bd2c200b674d5d8b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7950653bc2f1f871b1b31bf0eb67a0fa6e9132d1b0abadf8bd2c200b674d5d8b->enter($__internal_7950653bc2f1f871b1b31bf0eb67a0fa6e9132d1b0abadf8bd2c200b674d5d8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "HelpDeskBundle:Resolution:resolutionCreate.html.twig"));

        $__internal_20cbe07206265a54f02ced0e952a2466d81717f0b38bef158d63108b168aca67 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_20cbe07206265a54f02ced0e952a2466d81717f0b38bef158d63108b168aca67->enter($__internal_20cbe07206265a54f02ced0e952a2466d81717f0b38bef158d63108b168aca67_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "HelpDeskBundle:Resolution:resolutionCreate.html.twig"));

        // line 1
        echo "<style type=\"text/css\">
    label {
        display: inline-block;
        width:125px;
        vertical-align: top;
       } 
    input, textarea {
       width:200px;
       }
    input:focus {
        border: 2px solid #000;
        background: #F3F3F3;
    }
    textarea:focus {
        border: 2px solid #000;
        background: #F3F3F3;
    }
</style>

<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
    </head>
    <body>
        <h1>Create resolution</h1>
        ";
        // line 27
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form');
        echo "
    </body>
</html>
";
        
        $__internal_7950653bc2f1f871b1b31bf0eb67a0fa6e9132d1b0abadf8bd2c200b674d5d8b->leave($__internal_7950653bc2f1f871b1b31bf0eb67a0fa6e9132d1b0abadf8bd2c200b674d5d8b_prof);

        
        $__internal_20cbe07206265a54f02ced0e952a2466d81717f0b38bef158d63108b168aca67->leave($__internal_20cbe07206265a54f02ced0e952a2466d81717f0b38bef158d63108b168aca67_prof);

    }

    public function getTemplateName()
    {
        return "HelpDeskBundle:Resolution:resolutionCreate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 27,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<style type=\"text/css\">
    label {
        display: inline-block;
        width:125px;
        vertical-align: top;
       } 
    input, textarea {
       width:200px;
       }
    input:focus {
        border: 2px solid #000;
        background: #F3F3F3;
    }
    textarea:focus {
        border: 2px solid #000;
        background: #F3F3F3;
    }
</style>

<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
    </head>
    <body>
        <h1>Create resolution</h1>
        {{ form(form) }}
    </body>
</html>
", "HelpDeskBundle:Resolution:resolutionCreate.html.twig", "/home/racso/Master/Frameworks/symfony-standard/src/HelpDeskBundle/Resources/views/Resolution/resolutionCreate.html.twig");
    }
}
