<?php

/* HelpDeskBundle:HelpDesk:list.html.twig */
class __TwigTemplate_272c7262974988dcd02a2f45cafdc33b9cabd5f73f9ba60acaeb593e1cf82588 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4d174f92a499969f918c7e912f33984d0a4ba2e5e2ce4b82606e6a9be8e253be = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4d174f92a499969f918c7e912f33984d0a4ba2e5e2ce4b82606e6a9be8e253be->enter($__internal_4d174f92a499969f918c7e912f33984d0a4ba2e5e2ce4b82606e6a9be8e253be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "HelpDeskBundle:HelpDesk:list.html.twig"));

        $__internal_b3d71f127a1b31742c87ea79190ce7b0a68c43070696a5ec2d4d52e47e6d5854 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b3d71f127a1b31742c87ea79190ce7b0a68c43070696a5ec2d4d52e47e6d5854->enter($__internal_b3d71f127a1b31742c87ea79190ce7b0a68c43070696a5ec2d4d52e47e6d5854_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "HelpDeskBundle:HelpDesk:list.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
    </head>
    <body>
        <h1>Incidences</h1>
        <table style=\"width:100%\">
            <tr>
                <th>User</th>
                <th>Email</th> 
                <th>Cause of incidence</th>
                <th>Description</th>
                <th>It's Dangerouse</th>
                <th>Date Created</th>
                <th>Edit</th>
                <th>Resolution</th>
            </tr>
            ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["incidences"] ?? $this->getContext($context, "incidences")));
        foreach ($context['_seq'] as $context["_key"] => $context["incidence"]) {
            // line 20
            echo "                <tr>
                    <td>";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["incidence"], "getUserName", array()));
            echo "</td>
                    <td>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["incidence"], "getUserEmail", array()));
            echo "</td> 
                    <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["incidence"], "getCause", array()));
            echo "</td>
                    <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["incidence"], "getDescription", array()));
            echo "</td>
                    <td>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["incidence"], "getItsDangerouse", array()));
            echo "</td>
                    <td>";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["incidence"], "getDateCreatedFormated", array()));
            echo "</td>
                    <td><button type=\"button\"
                        onclick=\"location.href='";
            // line 28
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("incidenceEdit", array("id" => $this->getAttribute($context["incidence"], "id", array()))), "html", null, true);
            echo "';\">Edit
                    </button></td>
                    <td>
                        ";
            // line 31
            if (($this->getAttribute($context["incidence"], "getFinished", array()) == false)) {
                // line 32
                echo "                            <button type=\"button\"
                                onclick=\"location.href='";
                // line 33
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("resolutionCreate", array("incidenceId" => $this->getAttribute($context["incidence"], "getId", array()))), "html", null, true);
                echo "';\">add
                            </button>
                        ";
            }
            // line 36
            echo "                    </td>
                </tr>
                ";
            // line 38
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["incidence"], "resolutions", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["resolution"]) {
                // line 39
                echo "                    <tr>
                        <td colspan=\"7\">
                        ";
                // line 41
                echo twig_escape_filter($this->env, $this->getAttribute($context["resolution"], "getSolution", array()));
                echo "
                        </td>
                        <td>
                        ";
                // line 44
                echo twig_escape_filter($this->env, $this->getAttribute($context["resolution"], "getDateCreatedFormated", array()));
                echo "
                        </td>
                    </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['resolution'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 48
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['incidence'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "        </table>

        <button type=\"button\"
            onclick=\"location.href='";
        // line 52
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("incidenceCreate");
        echo "';\">New incidence
        </button>
    </body>
</html>
";
        
        $__internal_4d174f92a499969f918c7e912f33984d0a4ba2e5e2ce4b82606e6a9be8e253be->leave($__internal_4d174f92a499969f918c7e912f33984d0a4ba2e5e2ce4b82606e6a9be8e253be_prof);

        
        $__internal_b3d71f127a1b31742c87ea79190ce7b0a68c43070696a5ec2d4d52e47e6d5854->leave($__internal_b3d71f127a1b31742c87ea79190ce7b0a68c43070696a5ec2d4d52e47e6d5854_prof);

    }

    public function getTemplateName()
    {
        return "HelpDeskBundle:HelpDesk:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 52,  128 => 49,  122 => 48,  112 => 44,  106 => 41,  102 => 39,  98 => 38,  94 => 36,  88 => 33,  85 => 32,  83 => 31,  77 => 28,  72 => 26,  68 => 25,  64 => 24,  60 => 23,  56 => 22,  52 => 21,  49 => 20,  45 => 19,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
    </head>
    <body>
        <h1>Incidences</h1>
        <table style=\"width:100%\">
            <tr>
                <th>User</th>
                <th>Email</th> 
                <th>Cause of incidence</th>
                <th>Description</th>
                <th>It's Dangerouse</th>
                <th>Date Created</th>
                <th>Edit</th>
                <th>Resolution</th>
            </tr>
            {% for incidence in incidences %}
                <tr>
                    <td>{{ incidence.getUserName|e }}</td>
                    <td>{{ incidence.getUserEmail|e }}</td> 
                    <td>{{ incidence.getCause|e }}</td>
                    <td>{{ incidence.getDescription|e }}</td>
                    <td>{{ incidence.getItsDangerouse|e }}</td>
                    <td>{{ incidence.getDateCreatedFormated|e }}</td>
                    <td><button type=\"button\"
                        onclick=\"location.href='{{ path('incidenceEdit', { 'id': incidence.id }) }}';\">Edit
                    </button></td>
                    <td>
                        {% if incidence.getFinished == false %}
                            <button type=\"button\"
                                onclick=\"location.href='{{ path('resolutionCreate', { 'incidenceId': incidence.getId }) }}';\">add
                            </button>
                        {% endif %}
                    </td>
                </tr>
                {% for resolution in incidence.resolutions %}
                    <tr>
                        <td colspan=\"7\">
                        {{ resolution.getSolution|e }}
                        </td>
                        <td>
                        {{ resolution.getDateCreatedFormated|e }}
                        </td>
                    </tr>
                {% endfor %}
            {% endfor %}
        </table>

        <button type=\"button\"
            onclick=\"location.href='{{ path('incidenceCreate')  }}';\">New incidence
        </button>
    </body>
</html>
", "HelpDeskBundle:HelpDesk:list.html.twig", "/home/racso/Master/Frameworks/symfony-standard/src/HelpDeskBundle/Resources/views/HelpDesk/list.html.twig");
    }
}
