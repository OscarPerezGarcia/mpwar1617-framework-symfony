<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_676ae07160102dbbd0fe670eb20a654c643784fd11c74c5185e820f740258686 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b9b3cf49509055e9ecb7a246b7682c7cc4020756f09be4b85d0878f9fad94b74 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b9b3cf49509055e9ecb7a246b7682c7cc4020756f09be4b85d0878f9fad94b74->enter($__internal_b9b3cf49509055e9ecb7a246b7682c7cc4020756f09be4b85d0878f9fad94b74_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_8d4bd0e68950d7461875f0a98fc30714df186f7dc12ba8e204c94247c75a83ed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8d4bd0e68950d7461875f0a98fc30714df186f7dc12ba8e204c94247c75a83ed->enter($__internal_8d4bd0e68950d7461875f0a98fc30714df186f7dc12ba8e204c94247c75a83ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b9b3cf49509055e9ecb7a246b7682c7cc4020756f09be4b85d0878f9fad94b74->leave($__internal_b9b3cf49509055e9ecb7a246b7682c7cc4020756f09be4b85d0878f9fad94b74_prof);

        
        $__internal_8d4bd0e68950d7461875f0a98fc30714df186f7dc12ba8e204c94247c75a83ed->leave($__internal_8d4bd0e68950d7461875f0a98fc30714df186f7dc12ba8e204c94247c75a83ed_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_1164892547b3d5b588f0a10df87f7c386b95159a21fa26f8028da73c4db60b96 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1164892547b3d5b588f0a10df87f7c386b95159a21fa26f8028da73c4db60b96->enter($__internal_1164892547b3d5b588f0a10df87f7c386b95159a21fa26f8028da73c4db60b96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_6846f6fdd6f6148cd75d9e358b027bc8870836e8d0cf86d5e02905be23f46979 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6846f6fdd6f6148cd75d9e358b027bc8870836e8d0cf86d5e02905be23f46979->enter($__internal_6846f6fdd6f6148cd75d9e358b027bc8870836e8d0cf86d5e02905be23f46979_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_6846f6fdd6f6148cd75d9e358b027bc8870836e8d0cf86d5e02905be23f46979->leave($__internal_6846f6fdd6f6148cd75d9e358b027bc8870836e8d0cf86d5e02905be23f46979_prof);

        
        $__internal_1164892547b3d5b588f0a10df87f7c386b95159a21fa26f8028da73c4db60b96->leave($__internal_1164892547b3d5b588f0a10df87f7c386b95159a21fa26f8028da73c4db60b96_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_7e443f548b5fef42bbdfcd6786e24e7835684ddf7cf81577ab193379ff45ccc0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7e443f548b5fef42bbdfcd6786e24e7835684ddf7cf81577ab193379ff45ccc0->enter($__internal_7e443f548b5fef42bbdfcd6786e24e7835684ddf7cf81577ab193379ff45ccc0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_f9272ed570cbb6745bc4f6d54b6843cee07dbeda5a7f224c360727a280f2e95c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f9272ed570cbb6745bc4f6d54b6843cee07dbeda5a7f224c360727a280f2e95c->enter($__internal_f9272ed570cbb6745bc4f6d54b6843cee07dbeda5a7f224c360727a280f2e95c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_f9272ed570cbb6745bc4f6d54b6843cee07dbeda5a7f224c360727a280f2e95c->leave($__internal_f9272ed570cbb6745bc4f6d54b6843cee07dbeda5a7f224c360727a280f2e95c_prof);

        
        $__internal_7e443f548b5fef42bbdfcd6786e24e7835684ddf7cf81577ab193379ff45ccc0->leave($__internal_7e443f548b5fef42bbdfcd6786e24e7835684ddf7cf81577ab193379ff45ccc0_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_0d89f1f44a30b32a14be75871c7c17eee982f6bd1e42c094022e9190e240a6fb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0d89f1f44a30b32a14be75871c7c17eee982f6bd1e42c094022e9190e240a6fb->enter($__internal_0d89f1f44a30b32a14be75871c7c17eee982f6bd1e42c094022e9190e240a6fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_7cc44ccba9297b5716023727192643582232906b89c03fc7819ab90855aa2b1e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7cc44ccba9297b5716023727192643582232906b89c03fc7819ab90855aa2b1e->enter($__internal_7cc44ccba9297b5716023727192643582232906b89c03fc7819ab90855aa2b1e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_7cc44ccba9297b5716023727192643582232906b89c03fc7819ab90855aa2b1e->leave($__internal_7cc44ccba9297b5716023727192643582232906b89c03fc7819ab90855aa2b1e_prof);

        
        $__internal_0d89f1f44a30b32a14be75871c7c17eee982f6bd1e42c094022e9190e240a6fb->leave($__internal_0d89f1f44a30b32a14be75871c7c17eee982f6bd1e42c094022e9190e240a6fb_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/home/racso/Master/Frameworks/symfony-standard/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
