<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_8d7851e10dd7aad790ec19aaa31cedf2ab6f56c13bfbe6354facb3527efe2e6e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5a63eda10822d853c5e626f2e77ea407b05c9099ab1043cbf1dc2a1b22f6eda9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5a63eda10822d853c5e626f2e77ea407b05c9099ab1043cbf1dc2a1b22f6eda9->enter($__internal_5a63eda10822d853c5e626f2e77ea407b05c9099ab1043cbf1dc2a1b22f6eda9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_7901a4179c4ded47230bb58e980ac639e466f90aa1c055ca8c836923ccae72a1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7901a4179c4ded47230bb58e980ac639e466f90aa1c055ca8c836923ccae72a1->enter($__internal_7901a4179c4ded47230bb58e980ac639e466f90aa1c055ca8c836923ccae72a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a63eda10822d853c5e626f2e77ea407b05c9099ab1043cbf1dc2a1b22f6eda9->leave($__internal_5a63eda10822d853c5e626f2e77ea407b05c9099ab1043cbf1dc2a1b22f6eda9_prof);

        
        $__internal_7901a4179c4ded47230bb58e980ac639e466f90aa1c055ca8c836923ccae72a1->leave($__internal_7901a4179c4ded47230bb58e980ac639e466f90aa1c055ca8c836923ccae72a1_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_0148999cf28b93816e728a65b794767fab3db8267e57d35b9300105144f331fd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0148999cf28b93816e728a65b794767fab3db8267e57d35b9300105144f331fd->enter($__internal_0148999cf28b93816e728a65b794767fab3db8267e57d35b9300105144f331fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_e09a94ee8eec91def5afbb766cab2d7c5344d0fb7f1115461be231d45378c927 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e09a94ee8eec91def5afbb766cab2d7c5344d0fb7f1115461be231d45378c927->enter($__internal_e09a94ee8eec91def5afbb766cab2d7c5344d0fb7f1115461be231d45378c927_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_e09a94ee8eec91def5afbb766cab2d7c5344d0fb7f1115461be231d45378c927->leave($__internal_e09a94ee8eec91def5afbb766cab2d7c5344d0fb7f1115461be231d45378c927_prof);

        
        $__internal_0148999cf28b93816e728a65b794767fab3db8267e57d35b9300105144f331fd->leave($__internal_0148999cf28b93816e728a65b794767fab3db8267e57d35b9300105144f331fd_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_8ad997c6a372c7c9b8ec5d0587a19cc85b99ec22f77d04e51dabf80cf1722664 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8ad997c6a372c7c9b8ec5d0587a19cc85b99ec22f77d04e51dabf80cf1722664->enter($__internal_8ad997c6a372c7c9b8ec5d0587a19cc85b99ec22f77d04e51dabf80cf1722664_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_fa82d5b1b387266ec533c9d45e7d01b4901c9288164ea133c2632ea802612f15 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fa82d5b1b387266ec533c9d45e7d01b4901c9288164ea133c2632ea802612f15->enter($__internal_fa82d5b1b387266ec533c9d45e7d01b4901c9288164ea133c2632ea802612f15_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_fa82d5b1b387266ec533c9d45e7d01b4901c9288164ea133c2632ea802612f15->leave($__internal_fa82d5b1b387266ec533c9d45e7d01b4901c9288164ea133c2632ea802612f15_prof);

        
        $__internal_8ad997c6a372c7c9b8ec5d0587a19cc85b99ec22f77d04e51dabf80cf1722664->leave($__internal_8ad997c6a372c7c9b8ec5d0587a19cc85b99ec22f77d04e51dabf80cf1722664_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_eac650b37544b195145e5f6632a06e80ce19668b4f1aa5b5d6d764c18413a7cd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eac650b37544b195145e5f6632a06e80ce19668b4f1aa5b5d6d764c18413a7cd->enter($__internal_eac650b37544b195145e5f6632a06e80ce19668b4f1aa5b5d6d764c18413a7cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_4e0183036f4f7fc06be2254935c3e85e61f562309dfb427440f3a3368a772598 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4e0183036f4f7fc06be2254935c3e85e61f562309dfb427440f3a3368a772598->enter($__internal_4e0183036f4f7fc06be2254935c3e85e61f562309dfb427440f3a3368a772598_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_4e0183036f4f7fc06be2254935c3e85e61f562309dfb427440f3a3368a772598->leave($__internal_4e0183036f4f7fc06be2254935c3e85e61f562309dfb427440f3a3368a772598_prof);

        
        $__internal_eac650b37544b195145e5f6632a06e80ce19668b4f1aa5b5d6d764c18413a7cd->leave($__internal_eac650b37544b195145e5f6632a06e80ce19668b4f1aa5b5d6d764c18413a7cd_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/racso/Master/Frameworks/symfony-standard/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
