<?php

/* HelpDeskBundle:Incidence:list.html.twig */
class __TwigTemplate_4ee365daeaeafac1e1f90d5cf355a8fb449c08cfbac9a82deeeb7f87dba53f26 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1e10016b388ac499d3ec2c9bee0400cf9abb8417acf0e2d1d5d5bccc15373420 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1e10016b388ac499d3ec2c9bee0400cf9abb8417acf0e2d1d5d5bccc15373420->enter($__internal_1e10016b388ac499d3ec2c9bee0400cf9abb8417acf0e2d1d5d5bccc15373420_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "HelpDeskBundle:Incidence:list.html.twig"));

        $__internal_1dfc9e4eee825a0bdd750f4bb709853d5ffb0ec3eb825c6920d36d3f548f4e55 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1dfc9e4eee825a0bdd750f4bb709853d5ffb0ec3eb825c6920d36d3f548f4e55->enter($__internal_1dfc9e4eee825a0bdd750f4bb709853d5ffb0ec3eb825c6920d36d3f548f4e55_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "HelpDeskBundle:Incidence:list.html.twig"));

        // line 1
        echo "<style type=\"text/css\">
    table {
       border:1px solid black;
       border-collapse:collapse;
       width:100%; 
       height:auto; 
       } 
    tr.color {
        padding-top: 5px;
        background-color:#525151;
       }
    td {
        border:1px solid black;       
        padding:2px; 
       } 
    th{
        border:1px solid black;
        background: #525151;
        color: white;
        height:30px;
    }
    tr:hover {
          background-color: #ffff99;
    }

    .tr_solution_head{
        background: rgb(160, 146, 146);
    }
    .tr_solution{
        background: #e4d6d6;
    }
    .tr_solution_foot{
        background: rgb(160, 146, 146);   
        height: 5px;
    }
</style>

<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
    </head>
    <body>
        <h1>Incidences</h1>
        <table>
            <tr>
                <th>User</th>
                <th>Email</th> 
                <th>Cause of incidence</th>
                <th>Description</th>
                <th>It's Dangerouse</th>
                <th>Date Created</th>
                <th>Edit</th>
                <th>Resolution</th>
            </tr>
            ";
        // line 56
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["incidences"] ?? $this->getContext($context, "incidences")));
        foreach ($context['_seq'] as $context["_key"] => $context["incidence"]) {
            // line 57
            echo "                <tr>
                    <td>";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute($context["incidence"], "getUserName", array()));
            echo "</td>
                    <td>";
            // line 59
            echo twig_escape_filter($this->env, $this->getAttribute($context["incidence"], "getUserEmail", array()));
            echo "</td> 
                    <td>";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute($context["incidence"], "getCause", array()));
            echo "</td>
                    <td>";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute($context["incidence"], "getDescription", array()));
            echo "</td>
                    <td>";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute($context["incidence"], "getItsDangerouse", array()));
            echo "</td>
                    <td>";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute($context["incidence"], "getDateCreatedFormated", array()));
            echo "</td>
                    <td><button type=\"button\"
                        onclick=\"location.href='";
            // line 65
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("incidenceEdit", array("id" => $this->getAttribute($context["incidence"], "id", array()))), "html", null, true);
            echo "';\">Edit
                    </button></td>
                    <td>
                        ";
            // line 68
            if (($this->getAttribute($context["incidence"], "getFinished", array()) == false)) {
                // line 69
                echo "                            <button type=\"button\"
                                onclick=\"location.href='";
                // line 70
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("resolutionCreate", array("incidenceId" => $this->getAttribute($context["incidence"], "getId", array()))), "html", null, true);
                echo "';\">add
                            </button>
                        ";
            }
            // line 73
            echo "                    </td>
                </tr>
                ";
            // line 75
            if (($this->getAttribute($this->getAttribute($context["incidence"], "resolutions", array()), "count", array()) > 0)) {
                // line 76
                echo "                    <tr class=\"tr_solution_head\">
                        <td colspan=\"8\">
                            Solutions
                        </td>
                    </tr>
                ";
            }
            // line 82
            echo "
                ";
            // line 83
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["incidence"], "resolutions", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["resolution"]) {
                // line 84
                echo "                    <tr class=\"tr_solution\">
                        <td>
                        ";
                // line 86
                echo twig_escape_filter($this->env, $this->getAttribute($context["resolution"], "getDateCreatedFormated", array()));
                echo "
                        </td>
                        <td colspan=\"7\">
                        ";
                // line 89
                echo twig_escape_filter($this->env, $this->getAttribute($context["resolution"], "getSolution", array()));
                echo "
                        </td>
                    </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['resolution'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 93
            echo "                ";
            if (($this->getAttribute($this->getAttribute($context["incidence"], "resolutions", array()), "count", array()) > 0)) {
                // line 94
                echo "                    <tr class=\"tr_solution_foot\">
                        <td colspan=\"8\">
                        </td>
                    </tr>
                ";
            }
            // line 99
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['incidence'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 100
        echo "        </table>

        <button type=\"button\"
            onclick=\"location.href='";
        // line 103
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("incidenceCreate");
        echo "';\">New incidence
        </button>
    </body>
</html>";
        
        $__internal_1e10016b388ac499d3ec2c9bee0400cf9abb8417acf0e2d1d5d5bccc15373420->leave($__internal_1e10016b388ac499d3ec2c9bee0400cf9abb8417acf0e2d1d5d5bccc15373420_prof);

        
        $__internal_1dfc9e4eee825a0bdd750f4bb709853d5ffb0ec3eb825c6920d36d3f548f4e55->leave($__internal_1dfc9e4eee825a0bdd750f4bb709853d5ffb0ec3eb825c6920d36d3f548f4e55_prof);

    }

    public function getTemplateName()
    {
        return "HelpDeskBundle:Incidence:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  193 => 103,  188 => 100,  182 => 99,  175 => 94,  172 => 93,  162 => 89,  156 => 86,  152 => 84,  148 => 83,  145 => 82,  137 => 76,  135 => 75,  131 => 73,  125 => 70,  122 => 69,  120 => 68,  114 => 65,  109 => 63,  105 => 62,  101 => 61,  97 => 60,  93 => 59,  89 => 58,  86 => 57,  82 => 56,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<style type=\"text/css\">
    table {
       border:1px solid black;
       border-collapse:collapse;
       width:100%; 
       height:auto; 
       } 
    tr.color {
        padding-top: 5px;
        background-color:#525151;
       }
    td {
        border:1px solid black;       
        padding:2px; 
       } 
    th{
        border:1px solid black;
        background: #525151;
        color: white;
        height:30px;
    }
    tr:hover {
          background-color: #ffff99;
    }

    .tr_solution_head{
        background: rgb(160, 146, 146);
    }
    .tr_solution{
        background: #e4d6d6;
    }
    .tr_solution_foot{
        background: rgb(160, 146, 146);   
        height: 5px;
    }
</style>

<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
    </head>
    <body>
        <h1>Incidences</h1>
        <table>
            <tr>
                <th>User</th>
                <th>Email</th> 
                <th>Cause of incidence</th>
                <th>Description</th>
                <th>It's Dangerouse</th>
                <th>Date Created</th>
                <th>Edit</th>
                <th>Resolution</th>
            </tr>
            {% for incidence in incidences %}
                <tr>
                    <td>{{ incidence.getUserName|e }}</td>
                    <td>{{ incidence.getUserEmail|e }}</td> 
                    <td>{{ incidence.getCause|e }}</td>
                    <td>{{ incidence.getDescription|e }}</td>
                    <td>{{ incidence.getItsDangerouse|e }}</td>
                    <td>{{ incidence.getDateCreatedFormated|e }}</td>
                    <td><button type=\"button\"
                        onclick=\"location.href='{{ path('incidenceEdit', { 'id': incidence.id }) }}';\">Edit
                    </button></td>
                    <td>
                        {% if incidence.getFinished == false %}
                            <button type=\"button\"
                                onclick=\"location.href='{{ path('resolutionCreate', { 'incidenceId': incidence.getId }) }}';\">add
                            </button>
                        {% endif %}
                    </td>
                </tr>
                {% if incidence.resolutions.count > 0 %}
                    <tr class=\"tr_solution_head\">
                        <td colspan=\"8\">
                            Solutions
                        </td>
                    </tr>
                {% endif %}

                {% for resolution in incidence.resolutions %}
                    <tr class=\"tr_solution\">
                        <td>
                        {{ resolution.getDateCreatedFormated|e }}
                        </td>
                        <td colspan=\"7\">
                        {{ resolution.getSolution|e }}
                        </td>
                    </tr>
                {% endfor %}
                {% if incidence.resolutions.count > 0 %}
                    <tr class=\"tr_solution_foot\">
                        <td colspan=\"8\">
                        </td>
                    </tr>
                {% endif %}
            {% endfor %}
        </table>

        <button type=\"button\"
            onclick=\"location.href='{{ path('incidenceCreate')  }}';\">New incidence
        </button>
    </body>
</html>", "HelpDeskBundle:Incidence:list.html.twig", "/home/racso/Master/Frameworks/symfony-standard/src/HelpDeskBundle/Resources/views/Incidence/list.html.twig");
    }
}
