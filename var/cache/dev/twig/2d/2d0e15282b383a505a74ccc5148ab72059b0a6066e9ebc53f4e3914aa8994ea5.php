<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_57eac7e57ee9c10f4f48966119caf00ca08103952a8e037d78b3a81a68618438 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e6bf1960223eda0548735058aa80413796f3d5606b3540a6e4b8e97c7f0131b8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e6bf1960223eda0548735058aa80413796f3d5606b3540a6e4b8e97c7f0131b8->enter($__internal_e6bf1960223eda0548735058aa80413796f3d5606b3540a6e4b8e97c7f0131b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_7a42486e47f65ee87c61cd124546090550b031aec86d62c1511963dda0406244 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7a42486e47f65ee87c61cd124546090550b031aec86d62c1511963dda0406244->enter($__internal_7a42486e47f65ee87c61cd124546090550b031aec86d62c1511963dda0406244_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e6bf1960223eda0548735058aa80413796f3d5606b3540a6e4b8e97c7f0131b8->leave($__internal_e6bf1960223eda0548735058aa80413796f3d5606b3540a6e4b8e97c7f0131b8_prof);

        
        $__internal_7a42486e47f65ee87c61cd124546090550b031aec86d62c1511963dda0406244->leave($__internal_7a42486e47f65ee87c61cd124546090550b031aec86d62c1511963dda0406244_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_2fde70ecc51bbede093514abd3f9ad0a6e1ab391525728ca2bfd8460c2df6511 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2fde70ecc51bbede093514abd3f9ad0a6e1ab391525728ca2bfd8460c2df6511->enter($__internal_2fde70ecc51bbede093514abd3f9ad0a6e1ab391525728ca2bfd8460c2df6511_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_325af28aa20b97034797ba5d4112f2c015f2dcd035d0ad9e9c5e45aa38d5b266 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_325af28aa20b97034797ba5d4112f2c015f2dcd035d0ad9e9c5e45aa38d5b266->enter($__internal_325af28aa20b97034797ba5d4112f2c015f2dcd035d0ad9e9c5e45aa38d5b266_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_325af28aa20b97034797ba5d4112f2c015f2dcd035d0ad9e9c5e45aa38d5b266->leave($__internal_325af28aa20b97034797ba5d4112f2c015f2dcd035d0ad9e9c5e45aa38d5b266_prof);

        
        $__internal_2fde70ecc51bbede093514abd3f9ad0a6e1ab391525728ca2bfd8460c2df6511->leave($__internal_2fde70ecc51bbede093514abd3f9ad0a6e1ab391525728ca2bfd8460c2df6511_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_9e12f28ca331e0e8bbfc36e4c9390a75406a7f82cfb9c510c1ec92a847ea2d70 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9e12f28ca331e0e8bbfc36e4c9390a75406a7f82cfb9c510c1ec92a847ea2d70->enter($__internal_9e12f28ca331e0e8bbfc36e4c9390a75406a7f82cfb9c510c1ec92a847ea2d70_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_e9c739ae87e3452d8e116acb5b2c2c207dd35a2110801c1839a22263b8665c30 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e9c739ae87e3452d8e116acb5b2c2c207dd35a2110801c1839a22263b8665c30->enter($__internal_e9c739ae87e3452d8e116acb5b2c2c207dd35a2110801c1839a22263b8665c30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_e9c739ae87e3452d8e116acb5b2c2c207dd35a2110801c1839a22263b8665c30->leave($__internal_e9c739ae87e3452d8e116acb5b2c2c207dd35a2110801c1839a22263b8665c30_prof);

        
        $__internal_9e12f28ca331e0e8bbfc36e4c9390a75406a7f82cfb9c510c1ec92a847ea2d70->leave($__internal_9e12f28ca331e0e8bbfc36e4c9390a75406a7f82cfb9c510c1ec92a847ea2d70_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_d21ab143e17cda803c7e53d1e12c8dd2dfb720b12244004921880dac710cb4e1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d21ab143e17cda803c7e53d1e12c8dd2dfb720b12244004921880dac710cb4e1->enter($__internal_d21ab143e17cda803c7e53d1e12c8dd2dfb720b12244004921880dac710cb4e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_e8d3387b882dbaefb7a26c7a3239b0469291a15767c49907863735eef1f11d51 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e8d3387b882dbaefb7a26c7a3239b0469291a15767c49907863735eef1f11d51->enter($__internal_e8d3387b882dbaefb7a26c7a3239b0469291a15767c49907863735eef1f11d51_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_e8d3387b882dbaefb7a26c7a3239b0469291a15767c49907863735eef1f11d51->leave($__internal_e8d3387b882dbaefb7a26c7a3239b0469291a15767c49907863735eef1f11d51_prof);

        
        $__internal_d21ab143e17cda803c7e53d1e12c8dd2dfb720b12244004921880dac710cb4e1->leave($__internal_d21ab143e17cda803c7e53d1e12c8dd2dfb720b12244004921880dac710cb4e1_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/home/racso/Master/Frameworks/symfony-standard/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
