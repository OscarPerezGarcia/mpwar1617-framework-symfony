<?php
NAMESPACE HelpDeskBundle\Event;
USE HelpDeskBundle\Entity\Incidence;
USE Symfony\Component\EventDispatcher\Event;

class IncidenceFinalizedEvent extends Event{
	private $incidence;

	public function __construct(Incidence $incidence){
		$this->incidence = $incidence;
	}

	public function getIncidence():Incidence{
		return $this->incidence;
	}
}