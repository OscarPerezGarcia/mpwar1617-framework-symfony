<?php

NAMESPACE HelpDeskBundle\Entity;
USE Doctrine\ORM\Mapping AS ORM;
USE \DateTime;

/** 
* @ORM\Entity
* @ORM\Table(name="resolution")
*/
class Resolution {
	/** 
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	private $id;

	/**
     * @ORM\ManyToOne(targetEntity="Incidence", inversedBy="resolutions")
     * @ORM\JoinColumn(name="incidenceId", referencedColumnName="id")
     */
    private $incidence;

	/** 
	* @ORM\Column(type="string", length=1000)
	*/
	private $solution;

	/** 
	* @ORM\Column(type="boolean")
	*/
	private $finished;

	/** 
	* @ORM\Column(type="datetime")
	*/
	private $dateCreated;

	private function __construct
	(
		Incidence $incidence,
		String $solution,
		Bool $finished
	)
	{
		$this->incidence = $incidence;
		$this->solution = $solution;
		$this->finished = $finished;
	}

	static function create(Incidence $incidence, String $solution, Bool $finished):Resolution{
		$resolution = new Resolution($incidence, $solution, $finished);
		return $resolution;
	}

	public function getId():Int{
		return $this->id;
	}
	public function setId(Int $id){
		$this->id = $id;
	}

	public function getIncidence():Incidence{
		return $this->incidence;
	}
	public function setIncidence(Incidence $incidence){
		$this->incidence = $incidence;
	}

	public function getSolution():String{
		return $this->solution;
	}
	public function setSolution(String $solution){
		$this->solution = $solution;
	}

	public function getFinished():Bool{
		return $this->finished;
	}
	public function setFinished(Bool $finished){
		$this->finished = $finished;
	}

	public function getDateCreated():DateTime{
		return $this->dateCreated;
	}
	public function setDateCreated(DateTime $dateCreated){
		$this->dateCreated = $dateCreated;
	}
	public function getDateCreatedFormated():String{
		return $this->dateCreated->format('Y-m-d H:i:s');
	}
}