<?php

NAMESPACE HelpDeskBundle\Controller;

USE \DateTime;
USE HelpDeskBundle\Entity\Incidence;
USE HelpDeskBundle\Entity\Resolution;
USE HelpDeskBundle\Form\Type\ResolutionType;
USE HelpDeskBundle\Event\IncidenceFinalizedEvent;
USE Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
USE Symfony\Bundle\FrameworkBundle\Controller\Controller;
USE Symfony\Component\HttpFoundation\Request;
USE Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ResolutionController extends Controller
{
    /**
    * @Route("/resolution/create/{incidenceId}", name="resolutionCreate")
    * @Template
    */
    public function resolutionCreateAction(Request $request, Int $incidenceId)
    {
        $em = $this->getDoctrine()->getManager();
        
        $repositoryIncidence = $em->getRepository("HelpDeskBundle:Incidence");
        $incidence = $repositoryIncidence->find($incidenceId);
        $resolution = Resolution::create($incidence, '', false);

        $form = $this->createForm(ResolutionType::class, $resolution);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dataCreated = new \DateTime("now");
            $resolution = $form->getData();
            $resolution->setDateCreated($dataCreated);

            $em = $this->getDoctrine()->getManager();
            $em->persist($resolution);
            $em->flush();

            if ($resolution->getFinished()){
                $this->finalizeIncidence($incidenceId);
            }

            return $this->redirect($this->generateUrl('incidenceList'));
        }

        return array('form' => $form->createView());
    }

    private function LanzarEventoFinalizedIncidence(Incidence $incidence){
        $incidenceFinalizedEvent = new IncidenceFinalizedEvent($incidence);
        $event = $this->get("event_dispatcher");
        $event->dispatch('incidence_finalized', $incidenceFinalizedEvent);
    }

    private function finalizeIncidence(int $incidenceId){
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository("HelpDeskBundle:Incidence");
        $incidence = $repository->find($incidenceId);
        $incidence->setFinished(true);
        $em->flush();

        $this->LanzarEventoFinalizedIncidence($incidence);
    }
}
