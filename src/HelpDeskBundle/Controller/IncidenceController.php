<?php

NAMESPACE HelpDeskBundle\Controller;

USE \DateTime;
USE HelpDeskBundle\Entity\Incidence;
USE HelpDeskBundle\Entity\Resolution;
USE HelpDeskBundle\Form\Type\IncidenceType;
USE HelpDeskBundle\Form\Type\ResolutionType;
USE HelpDeskBundle\Event\IncidenceCreatedEvent;
USE Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
USE Symfony\Bundle\FrameworkBundle\Controller\Controller;
USE Symfony\Component\HttpFoundation\Request;
USE Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class IncidenceController extends Controller
{
    /**
    * @Route("/incidence/delete/{id}", name="incidenceDelete")
    */
    public function deleteIncidenceAction(Int $id)
    {   
        $em = $this->getDoctrine()->getManager();

        $repository = $em->getRepository("HelpDeskBundle:Incidence");
        $incidence = $repository->find($id);

        $em->remove($incidence);
        $em->flush();

        $this->LanzarEventoDeleted($incidence);

        return new Response("Se ha eliminado la incidencia con id {$id}");
    }

    /**
    * @Route("/helpDesk/incidenceList", name="incidenceList")
    * @Template
    */
    public function listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $repository = $em->getRepository("HelpDeskBundle:Incidence");
        $incidences = $repository->findAll();
        return array('incidences' => $incidences);
    }

    /**
    * @Route("/incidence/edit/{id}", name="incidenceEdit")
    * @Template
    */
    public function editAction(Request $request, int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository("HelpDeskBundle:Incidence");
        $incidence = $repository->find($id);
        
        $form = $this->createForm(IncidenceType::class, $incidence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $incidence = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($incidence);
            $em->flush();
            return $this->redirect($this->generateUrl('incidenceList'));
        }

        return array('form' => $form->createView());
    }

    /**
    * @Route("/helpDesk/incidenceCreate", name="incidenceCreate")
    * @Template
    */
    public function incidenceCreateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository("HelpDeskBundle:Incidence");
        $incidence = Incidence::create('','',  '', '', false);
        
        $form = $this->createForm(IncidenceType::class, $incidence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dataCreated = new \DateTime("now");
            $incidence = $form->getData();
            $incidence->setDateCreated($dataCreated);
            $incidence->setFinished(false);

            $em = $this->getDoctrine()->getManager();
            $em->persist($incidence);
            $em->flush();

            $this->LanzarEventoCreateIncidence($incidence);
            return $this->redirect($this->generateUrl('incidenceList'));
        }

        return array('form' => $form->createView());
    }

    private function LanzarEventoCreateIncidence(Incidence $incidence){
        $incidenceCreatedEvent = new IncidenceCreatedEvent($incidence);
        $event = $this->get("event_dispatcher");
        $event->dispatch('incidence_created', $incidenceCreatedEvent);
    }
}
