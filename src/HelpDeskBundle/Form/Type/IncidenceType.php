<?php
NAMESPACE HelpDeskBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class IncidenceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userName', TextType::class, array('label' => "User name :"))
            ->add('userEmail', EmailType::class, array('label' => "User Email :"))
            ->add('cause', TextType::class, array('label' => "Cause :"))
            ->add('Description', TextareaType::class, array('label' => "Description :"))
            ->add('itsDangerouse', CheckboxType::class, array('label' => "It's Dangerouse?", 'required' => false))
            ->add('save', SubmitType::class, array('label' => 'save'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'HelpDeskBundle\Entity\Incidence',
        ));
    }
}