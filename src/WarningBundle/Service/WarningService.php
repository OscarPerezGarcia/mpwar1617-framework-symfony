<?php
NAMESPACE WarningBundle\Service;
USE HelpDeskBundle\Entity\Incidence;

final class WarningService{
	public function incidenceCreated(Incidence $incidence){
        $to      = $incidence->getUserEmail();
        $subject = 'New incidence created';
        $message = "hello, You have created the incidence Nº {$incidence->getId()}";

        mail($to, $subject, $message);
	}

    public function incidenceFinalized(Incidence $incidence){
        $to      = $incidence->getUserEmail();
        $subject = 'Incidence finalized';
        $message = "hello, the incidence Nº {$incidence->getId()} have finished.";

        mail($to, $subject, $message);
    }
}