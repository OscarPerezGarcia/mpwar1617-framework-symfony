<?php
NAMESPACE WarningBundle\EventListener;
USE HelpDeskBundle\Event\IncidenceCreatedEvent;
USE HelpDeskBundle\Event\IncidenceFinalizedEvent;

class WarningEventListener{
	private $incidenceLogger;

	public function __construct($incidenceLogger){
		$this->incidenceLogger = $incidenceLogger;
	}

	public function incidenceCreated(IncidenceCreatedEvent $event){
        $this->incidenceLogger->incidenceCreated($event->getIncidence());
    }

    public function incidenceFinalized(IncidenceFinalizedEvent $event){
        $this->incidenceLogger->incidenceFinalized($event->getIncidence());
    }
}