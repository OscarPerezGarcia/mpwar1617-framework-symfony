# README #

### ¿Cual es su objetivo? ###

Consiste en una aplicación que permite añadir incidencias, y sobre estas, tantas resoluciones como se desen hasta que se de por terminada.

Cuando se genera la incidencia, se emite un evento el cual es capturado para enviar un correo electrónico al usuario que ha dado de alta el resgistro.

Cuando se finaliza una incidencia (en el momento de dar de alta una solución que finaliza el proceso), se emite un evento que es capturado para enviar un correo electrónico al usuario que dio de alta la incidencia.


### ¿Que rutas hay disponibles? ###

* helpDesk/incidenceList
  Muestra el listado de incidencias existntes. Desde la paǵina se pueden añadir incidencias, modificar las existentes y añadir soluciones.
* /helpDesk/incidenceCreate
  Accede a la página para crear una nueva incidencia.
* /incidence/edit/{Id}
  Accede a la página para editar la incidencia indicada.